﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DAF2020
{
    public partial class Amortz : Form
    {
        public Amortz()
        {
            InitializeComponent();
        }

        private void BtnAcept_Click(object sender, EventArgs e)
        {
            double interes = double.Parse(TxtInt.Text) / 100;
            double cuota=(double.Parse(txtPrestamo.Text)*(interes/(1-Math.Pow(1+interes,-double.Parse(txtPlazo.Text)))));
            int plazo = Convert.ToInt32(txtPlazo.Text);
            double saldo = double.Parse(txtPrestamo.Text);

            //calculos
            double saldoNew = saldo;
            
            for (int i = 0; i < plazo+1; i++)
            {
                double interesDeven = saldoNew * interes;
                double AmortizacionP = cuota - interesDeven;
                saldoNew = saldoNew - AmortizacionP;
                dgvDatos.Rows.Add();
                if (i == 0)
                {
                    //i.ToString(), p.ToString(), 0, 0);
                    dgvDatos.Rows[i].Cells[0].Value = i.ToString();
                    dgvDatos.Rows[i].Cells[1].Value = 0;
                    dgvDatos.Rows[i].Cells[2].Value = 0;
                    dgvDatos.Rows[i].Cells[3].Value = 0;
                    dgvDatos.Rows[i].Cells[4].Value = saldo;
                }
                else
                {
                    dgvDatos.Rows[i].Cells[0].Value = i.ToString();
                    dgvDatos.Rows[i].Cells[1].Value = AmortizacionP.ToString();
                    dgvDatos.Rows[i].Cells[2].Value = interesDeven.ToString();
                    dgvDatos.Rows[i].Cells[3].Value = cuota.ToString();
                    dgvDatos.Rows[i].Cells[4].Value = saldoNew.ToString();

                }

            }
            
        }
    }
}
