﻿namespace DAF2020
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dsDato = new System.Data.DataSet();
            this.dataTable1 = new System.Data.DataTable();
            this.dataColumn1 = new System.Data.DataColumn();
            this.dataColumn2 = new System.Data.DataColumn();
            this.dataColumn3 = new System.Data.DataColumn();
            this.dataColumn4 = new System.Data.DataColumn();
            this.bsDato = new System.Windows.Forms.BindingSource(this.components);
            this.btnDepre = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dsDato)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsDato)).BeginInit();
            this.SuspendLayout();
            // 
            // dsDato
            // 
            this.dsDato.DataSetName = "NewDataSet";
            this.dsDato.Tables.AddRange(new System.Data.DataTable[] {
            this.dataTable1});
            // 
            // dataTable1
            // 
            this.dataTable1.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn1,
            this.dataColumn2,
            this.dataColumn3,
            this.dataColumn4});
            this.dataTable1.TableName = "Dato";
            // 
            // dataColumn1
            // 
            this.dataColumn1.ColumnName = "Plazo";
            // 
            // dataColumn2
            // 
            this.dataColumn2.ColumnName = "Valor en Libro";
            // 
            // dataColumn3
            // 
            this.dataColumn3.ColumnName = "Depreciacion";
            // 
            // dataColumn4
            // 
            this.dataColumn4.ColumnName = "Valor Recuperado";
            // 
            // btnDepre
            // 
            this.btnDepre.Location = new System.Drawing.Point(50, 43);
            this.btnDepre.Name = "btnDepre";
            this.btnDepre.Size = new System.Drawing.Size(99, 23);
            this.btnDepre.TabIndex = 0;
            this.btnDepre.Text = "depreciacion";
            this.btnDepre.UseVisualStyleBackColor = true;
            this.btnDepre.Click += new System.EventHandler(this.BtnDepre_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(293, 43);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 1;
            this.button1.Text = "amrtizacioon";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.Button1_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(479, 450);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btnDepre);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.dsDato)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bsDato)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.BindingSource bsDato;
        private System.Data.DataSet dsDato;
        private System.Data.DataTable dataTable1;
        private System.Data.DataColumn dataColumn1;
        private System.Data.DataColumn dataColumn2;
        private System.Data.DataColumn dataColumn3;
        private System.Data.DataColumn dataColumn4;
        private System.Windows.Forms.Button btnDepre;
        private System.Windows.Forms.Button button1;
    }
}

