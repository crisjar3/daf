﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DAF2020
{
    public partial class Depreciacion : Form
    {
        public Depreciacion()
        {
            InitializeComponent();

        }
        

        private void BtnAcept_Click(object sender, EventArgs e)
        {
            if (convalidation()==false)
            {
                MessageBox.Show("profavor rellene todos los campos");
                return;
            }
            double p = double.Parse(TxtInv.Text);
            double deprec;
            double vs = p * 0.25;
            int plazo = Convert.ToInt32(TxtPlazo.Text);
            double interes = double.Parse(txtInt.Text) / 100;
            deprec = (double.Parse(TxtInv.Text)-vs) / double.Parse(TxtPlazo.Text);
            double inver = double.Parse(TxtInv.Text);
            
            double deprecAcum = double.Parse(TxtInv.Text) / double.Parse(TxtPlazo.Text);
            

            for (int i=0; i<plazo; i++)
            {
                
                if (i == 0)
                {
                    dgvDatos.Rows.Add();

                    dgvDatos.Rows[i].Cells[0].Value = i.ToString();
                    dgvDatos.Rows[i].Cells[1].Value = p;
                    dgvDatos.Rows[i].Cells[2].Value = 0;
                    dgvDatos.Rows[i].Cells[3].Value = 0;




                }
                else
                {

                    //DataGridViewRow fila = new DataGridViewRow();
                    //fila.CreateCells(dgvDatos);
                    //fila.Cells[0].Value = i.ToString();
                    //fila.Cells[1].Value = inver.ToString();
                    //fila.Cells[2].Value = deprec.ToString();
                    //fila.Cells[3].Value = deprecAcum.ToString();
                    //dgvDatos.Rows.Add(fila);
                    dgvDatos.Rows.Add();

                    dgvDatos.Rows[i].Cells[0].Value = i.ToString();
                    dgvDatos.Rows[i].Cells[1].Value = inver.ToString();
                    dgvDatos.Rows[i].Cells[2].Value = deprec.ToString();
                    dgvDatos.Rows[i].Cells[3].Value = deprecAcum.ToString();

                    deprecAcum = deprecAcum + deprec;
                }
                
            }


        }
        
        public Boolean convalidation()
        {
            if(txtInt.Equals("") || TxtInv.Equals("") ||TxtPlazo.Equals(""))
            {
                return false;
            }
            return true;
        }

        private void BtnCancel_Click(object sender, EventArgs e)
        {
            this.Dispose();
        }

        private void TxtPlazo_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!(char.IsNumber(e.KeyChar)) && (e.KeyChar != (char)Keys.Back))
            {
                MessageBox.Show("Solo se permiten numeros", "Advertencia", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                e.Handled = true;
                return;
            }
        }
    }
}
